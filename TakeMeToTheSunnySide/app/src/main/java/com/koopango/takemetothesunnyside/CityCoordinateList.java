package com.koopango.takemetothesunnyside;

/**
 * Created by degol on 6/6/2017.
 */

public interface CityCoordinateList {

    public String list = "  [\n" +
            "  {\n" +
            "    \"id\": 2950159,\n" +
            "    \"name\": \"Berlin\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 13.41053,\n" +
            "      \"lat\": 52.524368\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2911298,\n" +
            "    \"name\": \"Hamburg\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 10,\n" +
            "      \"lat\": 53.549999\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2922582,\n" +
            "    \"name\": \"Garching bei Munchen\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 11.65,\n" +
            "      \"lat\": 48.25\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2886242,\n" +
            "    \"name\": \"Koeln\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 6.95,\n" +
            "      \"lat\": 50.933331\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2925533,\n" +
            "    \"name\": \"Frankfurt am Main\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 8.68333,\n" +
            "      \"lat\": 50.116669\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2928810,\n" +
            "    \"name\": \"Essen\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.01667,\n" +
            "      \"lat\": 51.450001\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2935517,\n" +
            "    \"name\": \"Dortmund\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.45,\n" +
            "      \"lat\": 51.51667\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2825297,\n" +
            "    \"name\": \"Stuttgart\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 9.17702,\n" +
            "      \"lat\": 48.782318\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2934246,\n" +
            "    \"name\": \"Dusseldorf\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 6.77616,\n" +
            "      \"lat\": 51.221722\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2944387,\n" +
            "    \"name\": \"Bremen\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 8.83333,\n" +
            "      \"lat\": 53.083328\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2910831,\n" +
            "    \"name\": \"Hannover\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 9.73322,\n" +
            "      \"lat\": 52.370522\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2934691,\n" +
            "    \"name\": \"Duisburg\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 6.75,\n" +
            "      \"lat\": 51.433331\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2861650,\n" +
            "    \"name\": \"Nuremberg\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 11.06833,\n" +
            "      \"lat\": 49.447781\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2879139,\n" +
            "    \"name\": \"Leipzig\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 12.37129,\n" +
            "      \"lat\": 51.339619\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2935022,\n" +
            "    \"name\": \"Dresden\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 13.73832,\n" +
            "      \"lat\": 51.050892\n" +
            "    }\n" +
            "  },\n" +
            "   {\n" +
            "    \"id\": 2947416,\n" +
            "    \"name\": \"Bochum\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.21667,\n" +
            "      \"lat\": 51.48333\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2805753,\n" +
            "    \"name\": \"Wuppertal\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.18333,\n" +
            "      \"lat\": 51.26667\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2949186,\n" +
            "    \"name\": \"Bielefeld\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 8.53333,\n" +
            "      \"lat\": 52.033329\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2946447,\n" +
            "    \"name\": \"Bonn\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.1,\n" +
            "      \"lat\": 50.73333\n" +
            "    }\n" +
            "  },\n" +
            "   {\n" +
            "    \"id\": 2873891,\n" +
            "    \"name\": \"Mannheim\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 8.46472,\n" +
            "      \"lat\": 49.488331\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2892794,\n" +
            "    \"name\": \"Karlsruhe\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 8.38583,\n" +
            "      \"lat\": 49.004719\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2921466,\n" +
            "    \"name\": \"Gelsenkirchen\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.05,\n" +
            "      \"lat\": 51.51667\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2809346,\n" +
            "    \"name\": \"Wiesbaden\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 8.25,\n" +
            "      \"lat\": 50.083328\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2867543,\n" +
            "    \"name\": \"Muenster\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 7.62571,\n" +
            "      \"lat\": 51.96236\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2869894,\n" +
            "    \"name\": \"Monchengladbach\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 6.43333,\n" +
            "      \"lat\": 51.200001\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2940132,\n" +
            "    \"name\": \"Chemnitz\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 12.91667,\n" +
            "      \"lat\": 50.833328\n" +
            "    }\n" +
            "  },\n" +
            "   {\n" +
            "    \"id\": 2954172,\n" +
            "    \"name\": \"Augsburg\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 10.88333,\n" +
            "      \"lat\": 48.366669\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2945024,\n" +
            "    \"name\": \"Braunschweig\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 10.53333,\n" +
            "      \"lat\": 52.26667\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 3247449,\n" +
            "    \"name\": \"Aachen\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 6.08342,\n" +
            "      \"lat\": 50.776642\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2884509,\n" +
            "    \"name\": \"Krefeld\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 6.56667,\n" +
            "      \"lat\": 51.333328\n" +
            "    }\n" +
            "  },\n" +
            "    {\n" +
            "    \"id\": 2844588,\n" +
            "    \"name\": \"Rostock\",\n" +
            "    \"country\": \"DE\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 12.14049,\n" +
            "      \"lat\": 54.088699\n" +
            "    }\n" +
            "  }\n" +
            "  ]";
}
