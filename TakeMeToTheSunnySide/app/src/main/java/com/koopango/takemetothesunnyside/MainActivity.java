package com.koopango.takemetothesunnyside;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.koopango.takemetothesunnyside.AppNetworkAccess.NetworkUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private long delayInSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentTime();
        addListenerOnButton();
    }

    //Handler usage for current time
    private void currentTime() {
     //Add here a functionality to get the time every second and update the UI
    }

    private void findListWithClearSky(List<CityCoordinateID> nearestCities, String condition) {

        Runnable weatherJob = (BackgroundThreadWeather) setUpThread(nearestCities, condition);
        Thread weatherThread = new Thread(weatherJob);
        weatherThread.start();
    }

    private BackgroundThreadWeather setUpThread(List<CityCoordinateID> nearestCities, String condition) {

        return new BackgroundThreadWeather(this, condition, nearestCities, new NetworkUtil.IApiAccessResponse() {
            @Override
            public void postResult(final List<CityCoordinateID> nearestCities1) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateList(nearestCities1);

                    }
                });


            }
        });
    }

    private void findListSingleThreadMock(List<CityCoordinateID> nearestCities, String condition) {

        mockedDelay();
        findListWithClearSky(nearestCities, condition);
    }

    private void findListMultiThreadMock(List<CityCoordinateID> nearestCities, String condition) {

    //Implement a call to mockedDelay in a different delay //TODO
        findListWithClearSky(nearestCities, condition);
    }

    private void updateList(List<CityCoordinateID> nearestCities) {


        TextView firstWinner = (TextView) findViewById(R.id.firstWinner);
        TextView secondWinner = (TextView) findViewById(R.id.second_winner);
        TextView thirdWinner = (TextView) findViewById(R.id.third_winner);
        TextView firstWinnerDistance = (TextView) findViewById(R.id.firstWinnerDistance);
        TextView secondWinnerDistance = (TextView) findViewById(R.id.second_winner_distance);
        TextView thirdWinnerDistance = (TextView) findViewById(R.id.third_winner_distance);
        TextView delay = (TextView) findViewById(R.id.delay);

        firstWinner.setText(((nearestCities.size() > 0) ? (nearestCities.get(0).getName()) : "Sorry"));
        firstWinnerDistance.setText(((nearestCities.size() > 0) ?
                (String.valueOf((int) nearestCities.get(0).getDistance() / 1000) + " km") : "no result"));

        secondWinner.setText(((nearestCities.size() > 1) ? (nearestCities.get(1).getName()) : "Sorry"));
        secondWinnerDistance.setText(((nearestCities.size() > 1) ?
                (String.valueOf((int) nearestCities.get(1).getDistance() / 1000) + " km") : "no result"));

        thirdWinner.setText(((nearestCities.size() > 2) ? (nearestCities.get(2).getName()) : "Sorry"));
        thirdWinnerDistance.setText(((nearestCities.size() > 2) ?
                (String.valueOf((int) nearestCities.get(2).getDistance() / 1000) + " km") : "no result"));

        delay.setText(String.valueOf((System.currentTimeMillis()) - delayInSeconds) + " ms");


    }

    private void mockedDelay() {

        CoordinateDistanceUtil util = new CoordinateDistanceUtil();
        List<CityCoordinateID> cittList;
        int i = 3000;

        while (i > 0) {

            cittList = util.convertFromJSON();
            i--;
        }

    }

    public void addListenerOnButton() {

        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        Button btnMultipleDisplay = (Button) findViewById(R.id.multithread);
        Button btnSingleThread = (Button) findViewById(R.id.singleThread);
        final CoordinateDistanceUtil util = new CoordinateDistanceUtil();
        final ArrayList<CityCoordinateID> cityList = (ArrayList<CityCoordinateID>) util.convertFromJSON();

        btnSingleThread.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                delayInSeconds = System.currentTimeMillis();
                String lat = ((EditText) findViewById(R.id.lat)).getText().toString();
                String lon = ((EditText) findViewById(R.id.lon)).getText().toString();
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);

                if (lat.isEmpty() || lon.isEmpty()) {
                    lat = "54";
                    lon = "12";

                }

                List<CityCoordinateID> nearestCities = util.findProximity(cityList, Double.valueOf(lat), Double.valueOf(lon));
                findListSingleThreadMock(nearestCities, String.valueOf(radioButton.getText()));

            }


        });


        btnMultipleDisplay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                delayInSeconds = System.currentTimeMillis();
                String lat = ((EditText) findViewById(R.id.lat)).getText().toString();
                String lon = ((EditText) findViewById(R.id.lon)).getText().toString();
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);

                if (lat.isEmpty() || lon.isEmpty()) {
                    lat = "54";
                    lon = "12";

                }

                List<CityCoordinateID> nearestCities = util.findProximity(cityList, Double.valueOf(lat), Double.valueOf(lon));
                findListMultiThreadMock(nearestCities, String.valueOf(radioButton.getText()));


            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
