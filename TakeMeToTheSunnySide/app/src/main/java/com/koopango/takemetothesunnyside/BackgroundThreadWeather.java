package com.koopango.takemetothesunnyside;

import android.content.Context;


import com.koopango.takemetothesunnyside.AppNetworkAccess.NetworkAccessConfig;
import com.koopango.takemetothesunnyside.AppNetworkAccess.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

import static com.koopango.takemetothesunnyside.Constants.*;

/**
 * Created by degol on 5/29/2017.
 */

public class BackgroundThreadWeather implements Runnable {

    private final NetworkUtil.IApiAccessResponse responseInterface;
    private final List<CityCoordinateID> cityList;
    private final Context context;
    private final String conditon;

    public BackgroundThreadWeather(Context context, String condition, List<CityCoordinateID> cityList, NetworkUtil.IApiAccessResponse responseInterface) {
        this.cityList = cityList;
        this.responseInterface = responseInterface;
        this.context = context;
        this.conditon = condition;
    }


    @Override
    public void run() {

        runOnTheMainThread();

    }

    public void runOnTheMainThread() {
        NetworkUtil newWorkAccess = new NetworkUtil(this.context);
        NetworkAccessConfig config = new NetworkAccessConfig();
        List<CityCoordinateID> winnerList = new ArrayList<>(3);
        for (CityCoordinateID city : cityList) {
            configureHttpParameters(config, String.valueOf(city.getId()));
            newWorkAccess.sendRequest(config, city);
            if (city.getCondition().equals(this.conditon)) {
                winnerList.add(city);
            }
            if (winnerList.size() == 3) {
                break;
            }

        }
        responseInterface.postResult(winnerList);

    }


    private void configureHttpParameters(NetworkAccessConfig httpConfig, String id) {

        httpConfig.setRequestMethod(GET);
        httpConfig.setContentType(CONTENT_TYPE);
        httpConfig.setEncoding(ENCODING_UTF_8);
        httpConfig.setHost("http://api.openweathermap.org/data/2.5/weather?id=" + id + "&APPID=ac7da3d04f593fe1cf4eccf5daaef602");
    }
}
