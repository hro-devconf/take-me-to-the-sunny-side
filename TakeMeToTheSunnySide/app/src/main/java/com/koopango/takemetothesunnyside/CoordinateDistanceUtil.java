package com.koopango.takemetothesunnyside;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by degol on 6/6/2017.
 */

public class CoordinateDistanceUtil {

    public List findTheNearest() {
        return null;

    }

    public double distance(double lat1, double lat2, double lon1,
                           double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        return distance;
    }



    public List<CityCoordinateID> convertFromJSON() {
        List<CityCoordinateID> cityList = new ArrayList();
        try {
            JSONArray cityArray = new JSONArray(CityCoordinateList.list);
            for (int i = 0; i < cityArray.length(); i++) {
                double lon = Double.valueOf(cityArray.getJSONObject(i).getJSONObject("coord").get("lon").toString());
                double lat = Double.valueOf(cityArray.getJSONObject(i).getJSONObject("coord").get("lat").toString());
                int id = Integer.valueOf(cityArray.getJSONObject(i).get("id").toString());
                String name = cityArray.getJSONObject(i).get("name").toString();
                cityList.add(new CityCoordinateID(lon,lat,id, name));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cityList;
    }

    public List<CityCoordinateID> findProximity(List<CityCoordinateID> cityList, double lat, double lon) {

        for(CityCoordinateID city: cityList) {
            city.setDistance(distance(city.getLat(), lat, city.getLon(), lon));
        }

        Collections.sort((ArrayList)cityList);
        Collections.reverse(cityList);
        return cityList;
    }
}
