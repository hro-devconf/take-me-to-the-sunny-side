package com.koopango.takemetothesunnyside.AppNetworkAccess;

import android.content.Context;


import com.koopango.takemetothesunnyside.CityCoordinateID;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import static com.koopango.takemetothesunnyside.Constants.*;


/**
 * Created by degol on 5/23/2017.
 */

public class NetworkUtil implements AppNetworkAccess{
    private Context context;
    private NetworkUtil httpUtil;

    public NetworkUtil(Context context) {
        this.context = context;
    }

    @Override
    public CityCoordinateID sendRequest(NetworkAccessConfig config, CityCoordinateID city) {
        if ((config != null) && config.getRequestMethod().equals("GET")) {
            return sendGet(config, city);
        } else {
            return sendPost();
        }
    }

    private CityCoordinateID sendPost() {
        return null;
    }

    private static CityCoordinateID sendGet(NetworkAccessConfig config, CityCoordinateID city) {

        HttpURLConnection con = null;
        InputStream is = null;

        try {
            con = (HttpURLConnection) (new URL(config.getHost())).openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod(config.getRequestMethod());
            con.setRequestProperty("Content-Type", config.getContentType());

            con.connect();

            OutputStream wr = con.getOutputStream();
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = br.readLine()) != null)
                buffer.append(line + "rn");

            is.close();
            con.disconnect();
            String weather = buffer.toString();
            JSONObject jObject = new JSONObject(weather);
            String aJsonString = jObject.getString("weather");
            JSONArray jsonArray = new JSONArray(aJsonString);
            JSONObject listObject = new JSONObject(jsonArray.get(0).toString());
            String main = listObject.getString("main");
            city.setCondition(main);
            return city;
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Throwable t) {
            }
            try {
                con.disconnect();
            } catch (Throwable t) {
            }
        }
        return null;
    }


    public interface IApiAccessResponse {
        void postResult(List<CityCoordinateID> nearestCities);
    }
}
