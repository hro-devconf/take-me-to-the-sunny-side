package com.koopango.takemetothesunnyside.AppNetworkAccess;


import com.koopango.takemetothesunnyside.CityCoordinateID;

/**
 * Created by degol on 5/23/2017.
 */

public interface AppNetworkAccess {
    public CityCoordinateID sendRequest(NetworkAccessConfig config, CityCoordinateID city);
}

