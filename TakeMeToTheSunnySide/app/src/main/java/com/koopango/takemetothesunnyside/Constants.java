package com.koopango.takemetothesunnyside;

/**
 * Created by degol on 12/04/2017.
 */

public interface Constants {
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String POST = "POST";
    public static final String GET = "GET";




}
