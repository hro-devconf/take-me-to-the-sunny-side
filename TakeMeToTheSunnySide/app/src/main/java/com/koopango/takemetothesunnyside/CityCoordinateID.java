package com.koopango.takemetothesunnyside;

/**
 * Created by degol on 6/6/2017.
 */

public class CityCoordinateID implements Comparable {

    private final String name;
    private double distance;
    private double temperature;
    private final double lon;
    private final double lat;
    private final long id;

    public String getName() {
        return name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    private String condition;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    public long getId() {
        return id;
    }



    public CityCoordinateID (double lon, double lat, long id, String name) {
        this.lon = lon;
        this.lat = lat;
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof CityCoordinateID)) {
            return false;
        }

        CityCoordinateID user = (CityCoordinateID) o;

        return Double.valueOf(user.getDistance()).equals(this.distance);
    }

    //Idea from effective Java : Item 9
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + Double.valueOf(this.distance).hashCode();
        return result;
    }

    @Override
    public int compareTo(Object o) {
        CityCoordinateID user = (CityCoordinateID) o;

        if(user.getDistance() == this.distance){
            return 0;
        }
        else if(user.getDistance() > this.distance){
            return 1;
        }
        else{
            return -1;
        }
    }
}
